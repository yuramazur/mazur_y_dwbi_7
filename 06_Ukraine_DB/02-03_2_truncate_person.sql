use [people_ua_db]
go
alter table person
drop constraint [fk_person_region]
  
truncate table [person] 
go

alter table [person]
add
constraint [fk_person_region] 
foreign key([region_id]) 
references region(id)  
go
select * from person
use [master]