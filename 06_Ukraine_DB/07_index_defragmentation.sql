use [people_ua_db]
go
declare @index_name nvarchar(50)
declare @table_name nvarchar(50)
declare @persent float
declare @sql nvarchar(max)
select * from [index_frag_persent] where [avg_frag_persent] > 10
declare [defrag_index] cursor for 
                              select [index_name],[table_name],[avg_frag_persent]
							  from  [index_frag_persent]
open [defrag_index]
fetch next from [defrag_index] into @index_name,@table_name,@persent
while @@FETCH_STATUS = 0
BEGIN
     if @persent > 5 and @persent < 30 
	 begin
	       set @sql = CONCAT('ALTER INDEX ',@index_name,' ON ',@table_name,' REORGANIZE')
		   EXECUTE sp_executesql @sql
		   set @sql =''
		   print CONCAT('INDEX ',@index_name,' FROM TABLE ',@table_name,' WAS REORGANIZED!')	
	 end
	 if @persent > 30 
	 begin
	       set @sql = CONCAT('ALTER INDEX ',@index_name,' ON ',@table_name,' REBUILD')
		   EXECUTE sp_executesql @sql
		   set @sql =''
		   print CONCAT('INDEX ',@index_name,' FROM TABLE ',@table_name,' WAS REORGANIZED!')
	 end
     fetch next from [defrag_index] into @index_name,@table_name,@persent
END
close [defrag_index]
deallocate [defrag_index]
select * from [index_frag_persent]
go
use [master]							  