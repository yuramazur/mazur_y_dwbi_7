use [people_ua_db]
go
declare @person_count int = (select count(*) from [person])
declare @name_count int = (select count(*) from [name_list_identity])
declare @surname_count int = (select count(*) from [surname_list_identity])
declare @del_id int = 1
declare @upd_id int = 2
declare @name_id int
declare @surname_id int 
declare @count int = 0
declare @surname varchar(30)

while @count < 1000000
BEGIN
 while @del_id <> @upd_id
       begin
           set @del_id = (1+(select(FLOOR(RAND()*(@person_count-1)+1))))
           set @upd_id = (1+(select(FLOOR(RAND()*(@person_count-1)+1))))
       end
  set @name_id = (1+(select(FLOOR(RAND()*(@name_count-1)+1))))
  set @surname_id = (1+(select(FLOOR(RAND()*(@surname_count-1)+1))))
 -- UPDATE 
 update [person]
 set [id]        = (select max(id) from person)+1,
     [surname]   = (select [surname] from [surname_list_identity] where id = @surname_id),
	 [name]      = (select [name] from [name_list_identity] where id = @name_id),
	 [sex]       = (select [sex] from [name_list_identity] where id = @name_id),
	 [GUID]      =  NEWID()
	 
where [id] = @upd_id
-- DELETE 
delete [person]
where [id] = @del_id
set @count += 1
END
-- INSERT
set @count = 0
declare [insert_data] cursor for
                             select [surname] from [surname_list_identity]
open [insert_data]
while @count < 10000
BEGIN
fetch next from [insert_data] into @surname
while @@FETCH_STATUS = 0
  BEGIN
      set @name_id = (1+(select(FLOOR(RAND()*(@name_count-1)+1))))
      set @surname_id = (1+(select(FLOOR(RAND()*(@surname_count-1)+1))))
	  insert into [person]([id],[surname],[name],[sex],[GUID],[region_id],[birth_day],[date_creation]) values
		                   (((select max([id]) from [person])+1),
						     (select [surname] from [surname_list_identity] where id = @surname_id),
							 (select [name] from [name_list_identity] where id = @name_id),
							 (select [sex] from [name_list_identity] where id = @name_id),
							 NEWID(),1,'1990-01-01',GETDATE())   
      fetch next from [insert_data] into @surname
  END 
 set @count += 1
END
close [insert_data]
deallocate [insert_data]
print '-------------------------- ALL DONE ----------------------------------'
go
use [master]





