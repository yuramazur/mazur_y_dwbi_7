use [people_ua_db]
go
---------------- VIEWS  --------------------
create or alter view [m_name_view]
as 
select * from [name_list_identity]
where [sex] = 'm'
go
create or alter view [w_name_view]
as
select * from [name_list_identity]
where [sex] = 'w'


go
declare @m_names table (id int identity,[name] nvarchar(20)) 
insert into @m_names select [name] from [m_name_view]
declare @w_names table (id int identity,[name] nvarchar(20)) 
insert into @w_names select [name] from [w_name_view]
declare @name_count int = (select count(*) from [name_list_identity])
declare @m_name_count int = (select count(*) from @m_names)
declare @w_name_count int = (select count(*) from @w_names)
declare @region_count int = (select count(*) from [region])
declare @random_name table( id int, [name] nvarchar(20), [sex] nchar(1))
declare @birth_day varchar(10)
declare @region_id int
declare @surname nvarchar(20)
declare @amount int
declare @sex nchar(1)
declare @sex_insert nchar(1)
declare @name nvarchar(20)
declare @random_name_id int
declare @year int
declare @month int
declare @day int 
declare @days_amount int = 31
declare @count int = 0
declare [insert_data] cursor for 
                         select [surname],[sex],[amount] 
						 from [surname_list_identity]
						 
open [insert_data]
fetch next from [insert_data] into @surname, @sex , @amount
     while @@FETCH_STATUS = 0
	 BEGIN
	      while @amount > 0
		  BEGIN
		  set @year  = (1925+ (select FLOOR(RAND()*(76-1)+1)))
          set @month = (1+ (select FLOOR(RAND()*(12-1)+1)))
			   if @month = 2
                   begin
				   set @days_amount = 28
				   end
          set @day =   (1+ (select FLOOR(RAND()*(@days_amount-1)+1)))
	      set @birth_day = CONCAT(@year,'-',@month ,'-',@day) 
		  set @region_id = (1+ (select FLOOR(RAND()*(@region_count-1)+1)))
	      IF @sex IS NULL
	          BEGIN
	          set @random_name_id  = (1+(select FLOOR(RAND()*(@name_count-1)+1)))
              set @name = (select [name] from [name_list_identity] where id = @random_name_id)
	          set @sex_insert  = (select [sex]  from [name_list_identity] where id = @random_name_id)
	          END
	      ELSE
	      BEGIN
		      if @sex = 'm'
		         begin
		         set @name = (select [name] from @m_names where id = (1+(select FLOOR(RAND()*(@m_name_count-1)+1)))) 
		      end
		      if @sex = 'w'
		      begin
		         set @name = (select [name] from @w_names where id = (1+(select FLOOR(RAND()*(@w_name_count-1)+1))))
		      end
			  set @sex_insert = @sex
		  END
		  set @count += 1
		  insert into [person]([id],[surname],[name],[sex],[GUID],[region_id],[birth_day],[date_creation]) values
		                   (@count,@surname,@name,@sex_insert,NEWID(),@region_id,@birth_day,GETDATE())
	         set  @amount -= 1
		  END
	 fetch next from [insert_data] into @surname, @sex , @amount 
	 END
close [insert_data]
deallocate [insert_data]
PRINT ' ------------ ALL PERSONS WAS ADDED ------------ ' 
go
use [master]
