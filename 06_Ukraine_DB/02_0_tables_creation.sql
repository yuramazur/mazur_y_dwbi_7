use [people_ua_db]
go
create table [country](
[id] int not null default 1,
[name] varchar(128) default 'Ukraine',
constraint [pk_country] primary key 
nonclustered (id)
) 
go
create table [region](
[id] int not null identity,
[country_id] int not null default 1,[name] varchar(128)
constraint [pk_region] primary key 
nonclustered (id)
)
go
create table person(
[id] int not null ,
[surname] nvarchar(20) not null,
[name] nvarchar(20) not null,
[sex] nchar(1) check([sex]='m' or [sex] ='w') not null,
[GUID] uniqueidentifier not null default NEWID(),
[region_id] int not null default 1,
[birth_day] date not null,
[date_creation] datetime not null default GETDATE(),
[date_update] datetime null,
constraint [pk_person] primary key 
clustered (id)
)
go
alter table [region]
add
constraint [fk_region_country] 
foreign key([country_id]) 
references country(id)

alter table [person]
add
constraint [fk_person_region] 
foreign key([region_id]) 
references region(id)  
go
insert into country(id,[name]) values (1,'Ukraine')
go
insert into region(name) values ('³�������')
insert into region(name) values ('���������')
insert into region(name) values ('����������������')
insert into region(name) values ('�����������')
insert into region(name) values ('������������')
insert into region(name) values ('���������')
insert into region(name) values ('�����-����������')
insert into region(name) values ('�������')
insert into region(name) values ('ʳ������������')
insert into region(name) values ('���������')
insert into region(name) values ('��������')
insert into region(name) values ('�����������')
insert into region(name) values ('�������')
insert into region(name) values ('����������')
insert into region(name) values ('г��������')
insert into region(name) values ('�������')
insert into region(name) values ('������������')
insert into region(name) values ('���������')
insert into region(name) values ('����������')
insert into region(name) values ('�����������')
insert into region(name) values ('���������')
insert into region(name) values ('�����������')
insert into region(name) values ('����������')
insert into region(name) values ('��������� ��������� ����')
insert into region(name) values ('�.���')
insert into region(name) values ('�.�����������')

go
use [master]