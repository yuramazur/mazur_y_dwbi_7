use master
drop database if exists VIDEO_RENT
go
create database VIDEO_RENT
go
use VIDEO_RENT
go
create table movie(
id int identity(1,1) not null,
movie_name varchar(150) not null,
movie_year smallint not null,
amount int constraint df_movie_tape_amount default (0),
price money not null,
constraint pk_movie primary key clustered (id)
)
go
create table client(
id int identity(1,1) not null,
first_name varchar(50) not null,
last_name varchar(50) not null,
wollet money constraint df_client_wollet default(0),
constraint pk_client primary key clustered (id),
) 
go 
create table rent(
id int identity(1,1) not null,
rent_date date constraint df_rent_date default (getdate()) not null,
client_id int not null,
movie_id int not null,
amount int not null,
total_price money,
constraint pk_rent primary key clustered (id),
constraint fk_rent_client foreign key (client_id) references client(id),
constraint fk_rent_movie foreign key (movie_id) references movie(id)
)
