use VIDEO_RENT
go
declare @client int = 2, @movie int = 5, @amount int = 3, @price money 
select @price = price from movie where movie.id = @movie

if @amount > (select amount from movie where id = @movie) 
begin 
set @amount = (select amount from movie where id = @movie)
end  
begin
if (@price*@amount) > (select wollet from client where id = @client)
set @amount = round (((select wollet from client where id = @client)/@price),0,1)
end
if @amount > 0
begin
update movie
set amount = amount - @amount
where movie.id = @movie
insert rent(client_id,movie_id,amount,total_price) values (@client,@movie,@amount,(@amount*@price))
update client
set wollet = wollet-@amount*@price
where client.id = @Client
end
select * from rent
select * from movie
select * from client