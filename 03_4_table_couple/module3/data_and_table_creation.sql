use master
go
-- ----------------------  database creation
drop database if exists [Y_M_module_3]
create database [Y_M_module_3]
go
use [Y_M_module_3]
go
-- ----------------------  table department creation 
create table [department](
[id] int not null identity(1,1),
[inserted_date] datetime not null constraint def_dep_insert_date default GETDATE(),
[update_date] datetime ,
[date_of_creation] date not null constraint def_date_of_creaation default GETDATE(),
[name] varchar(10) not null,
[country] varchar(30) not null,
[city] varchar(30) not null,
[address] varchar(50) not null,
[phone] varchar(20) not null,
[e_mail] varchar(30) not null,
[GUID] uniqueidentifier constraint def_guid default (NEWID()),
[boss_id] bigint,
constraint pk_department primary key ([id]),
constraint un_name_country_city_address unique ([name],[country],[address]),
constraint un_phone unique ([phone]),
constraint un_email unique ([e_mail]),
constraint ch_dep_name check([name]<>'PRIME')
)
go
-- ----------------------  table employee creation  
create table [employee](
[id] bigint not null identity constraint pk_employee primary key,
[inserted_date] datetime not null constraint def_empl_insert_date default GETDATE(),
[update_date] datetime ,
[date_of_employment] date not null constraint def_date_of_employment default GETDATE(),
[first_name] varchar(30) not null constraint ch_empl_f_name check([first_name]<>'NO_NAME'),
[middle_name] varchar(30) constraint ch_empl_m_name check([middle_name]<>'NO_NAMEMOVICH') ,
[last_name] varchar(30) not null constraint ch_empl_l_name check([last_name]<>'NO_NAMENCO'),
[passport_number] varchar(10) not null constraint un_passport_number unique,
[identity_code] varchar(10) not null constraint un_identity_code unique,
[department_id] int,
[salary] money not null,
[bonus] money,
[total_money] as ([salary]+ISNULL([bonus],0)),
[expirience]  as CAST(ROUND(((DATEDIFF(DAY,[date_of_employment],GETDATE()))/365.5),2)as numeric(36,2))
)
go
-- ----------------------  foreign key creation
alter table [employee]
add constraint fk_employee_department 
foreign key ([department_id]) references [department]([id])
on delete no action
on update no action
alter table [department]
add constraint fk_department_employee 
foreign key ([boss_id]) references [employee]([id])
on delete no action
on update no action

go
use master