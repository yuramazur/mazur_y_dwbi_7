use [Y_M_module_3]
go
drop trigger if exists [trig].[tg_table_insert] 
drop trigger if exists [trig].[tg_table_update]
drop trigger if exists [trig].[tg_table_delete]  
go

------------------------------ INSERT TRIGGER

create or alter trigger [trig].[tg_table_insert] 
on [trig].[table]
after insert
as
begin
declare @operation varchar(7) = 'INSERT'
declare @update_date datetime = GETDATE();
insert into [trig].[table_operation](
[table_id],[value1], [value2], [value3],[value4],[value5],
[value6],[value7],[value8],[value9],[value10],
[value11],[value12],[value13],[value14],[value15],[update_date],[operation] 
)
select *,@update_date,@operation from INSERTED
end 
go

------------------------------ UPDATE TRIGGER

create or alter trigger [trig].[tg_table_update]
on [trig].[table]
after update
as
begin
declare @operation varchar(7) = 'UPDATE'
declare @update_date datetime = GETDATE();
insert into [trig].[table_operation](
[table_id],[value1], [value2], [value3],[value4],[value5],
[value6],[value7],[value8],[value9],[value10],
[value11],[value12],[value13],[value14],[value15],[update_date],[operation]  
)
select *, @update_date,@operation from INSERTED
end
go

------------------------------ DELETE TRIGGER

create or alter trigger [trig].[tg_table_delete] 
on [trig].[table]
for delete 
as
begin
declare @operation varchar(7) = 'DELETE'
declare @update_date datetime = GETDATE();
insert into [trig].[table_operation](
[table_id],[value1], [value2], [value3],[value4],[value5],
[value6],[value7],[value8],[value9],[value10],
[value11],[value12],[value13],[value14],[value15],[update_date],[operation]   
)
select *, @update_date,@operation from DELETED
end
go