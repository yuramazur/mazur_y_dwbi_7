use [Y_M_module_3]

drop table if exists [trig].[table]
drop table if exists [trig].[table_operation]
go
create table [trig].[table](
[id] int not null constraint pk_table primary key,
[value1] varchar (20),
[value2] varchar (20),
[value3] varchar (20),
[value4] varchar (20),
[value5] varchar (20),
[value6] varchar (20),
[value7] varchar (20),
[value8] varchar (20),
[value9] varchar (20),
[value10] varchar (20),
[value11] varchar (20),
[value12] varchar (20),
[value13] varchar (20),
[value14] varchar (20),
[value15] varchar (20)
)
go 
create table [trig].[table_operation](
[id] int not null identity constraint pk_employee_test_operation primary key,
[table_id] int,
[value1] varchar (20),
[value2] varchar (20),
[value3] varchar (20),
[value4] varchar (20),
[value5] varchar (20),
[value6] varchar (20),
[value7] varchar (20),
[value8] varchar (20),
[value9] varchar (20),
[value10] varchar (20),
[value11] varchar (20),
[value12] varchar (20),
[value13] varchar (20),
[value14] varchar (20),
[value15] varchar (20),
[update_date] datetime,
[operation] varchar(7)
)
go
use [master]