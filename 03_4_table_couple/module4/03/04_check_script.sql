use Y_M_module_3

go 
-- ------------------- INSERT TRIGGER CHECK
select * from [trig].[table]
select * from [trig].[table_operation]

go
-- ------------------- UPDATE TRIGGER CHECK
update [trig].[table]
set [value1] = 'updated'
where id in (1,3,5,7,9)
select * from [trig].[table]
select * from [trig].[table_operation]

go
-- ------------------- UPDATE TRIGGER CHECK
delete from [trig].[table]
where id in (2,4,6,8,10)
select * from [trig].[table]
select * from [trig].[table_operation]
-- ------------------- STATISTIC CHECK
go
update [trig].[table]
set [value1] = 'updated again!'
where [id] = 1
delete from [trig].[table]
where [id] = 1
select [table_id],[value1],[update_date],[operation]
from [trig].[table_operation]
where [table_id] = 1
order by [update_date] ASC 