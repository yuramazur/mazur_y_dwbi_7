use [Y_M_module_3]
go
drop view if exists [vw_employee_lviv]
go
create or alter view [vw_employee_lviv](
[id],
[date_of_employment],
[first_name],
[last_name],
[salary],
[expiriance],
[country],
[department_name],
[department_city],
[department_GUID]
)as
select
E.[id],
[date_of_employment],
[first_name],
[last_name],
[total_money],
[expirience],
D.[country],
D.[name],
D.[city],
D.[GUID]
from [employee] E join [department] D
on E.[department_id] = D.[id]
where D.[city] like 'Lviv'
with check option
go
select * from [vw_employee_lviv]
go 
use master