use [Y_M_module_3]
go 
drop view if exists [vw_employee] 
go  
create or alter view [vw_employee](
[date_of_employment],
[first_name],
[last_name],
[salary],
[expiriance],
[country],
[department_name],
[department_city],
[department_GUID]
)as
select
[date_of_employment],
[first_name],
[last_name],
[total_money],
[expirience],
D.[country],
D.[name],
D.[city],
D.[GUID]
from [employee] E join [department] D
on E.[department_id] = D.[id]
go
create or alter view [vw_edpartment](
[date_of_creation],
[name],
[country],
[city],
[address],
[phone],
[e_mail],
[boss_f_name],
[boss_l_name],
[boss_expiriance],
[boss_salary]
)
as
select 
[date_of_creation],
[name],
[country],
[city],
[address],
[phone],
[e_mail],
E.[first_name],
E.[last_name],
E.[expirience],
E.[total_money]
from [department] D join [employee] E
on D.[boss_id] = E.[id] 

go 
select * from [vw_employee]
go 
select * from [vw_edpartment]
go 
use [master]