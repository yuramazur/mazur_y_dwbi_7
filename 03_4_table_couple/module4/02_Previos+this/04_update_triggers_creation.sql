use [Y_M_module_3]
go
drop trigger if exists [tg_update_employee],[tg_update_department]
go
create or alter trigger [tg_update_employee]
on [employee]
after update
as
begin
update [employee]
set [update_date] = getdate()
where id in (select id from INSERTED)
end
go
create or alter trigger [tg_update_department]
on [department]
after update
as
begin
update [department]
set [update_date] = getdate()
where id in (select id from INSERTED)
end
go
use [master]

