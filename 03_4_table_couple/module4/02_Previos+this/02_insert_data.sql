use Y_M_module_3

-- ---------------------------------- department table insert data

--insert into [department]([date_of_creation],[name],[country],[city],[address],[phone],[e_mail]) 
--values ('2007-08-01','PRIME','Ukrain','Lviv','Konovalcia 31', '+3802492374437', 'prime_somecool@company.com') 
-- this insert dosen't match with check constraint 
insert into [department]([date_of_creation],[name],[country],[city],[address],[phone],[e_mail]) 
values ('2007-08-01','ALFA','Ukraine','Lviv','Konovalcia 31', '+3802492374437', 'prime_somecool@company.com')
insert into [department]([date_of_creation],[name],[country],[city],[address],[phone],[e_mail]) 
values ('2008-12-11','BETA','Ukraine','Kiev','Matushenka B 12', '+3804422974037', 'alfa_somecool@company.com')
insert into [department]([date_of_creation],[name],[country],[city],[address],[phone],[e_mail]) 
values ('2009-03-14','GAMA','Poland','Krakiv','Psheplacka 311a', '+481488881437', 'beta_somecool@company.com')
insert into [department]([date_of_creation],[name],[country],[city],[address],[phone],[e_mail]) 
values ('2009-08-26','SIGMA','USA','LA','Santa Del Ray 1', '+182414884437', 'gama_somecool@company.com')
insert into [department]([date_of_creation],[name],[country],[city],[address],[phone],[e_mail]) 
values ('2010-08-25','TETA','USA','New York','Manhattan 14 street', '+11212414888814', 'sigma_somecool@company.com')

go
-- ---------------------------------- employee table insert data

insert into [employee]([date_of_employment],[first_name],[middle_name],[last_name],[passport_number],[identity_code],[department_id],[salary],[bonus])
 values ('2006-12-12','Yurii','Jaroslavovich','Mazur','��1214184','1488145245',1,30000,1500)
insert into [employee]([date_of_employment],[first_name],[middle_name],[last_name],[passport_number],[identity_code],[department_id],[salary],[bonus])
 values ('2007-03-23','Pavlo','Antonovuch','Tarasov','�C1514184','1568145275',1,20000,500)
 insert into [employee]([date_of_employment],[first_name],[middle_name],[last_name],[passport_number],[identity_code],[department_id],[salary],[bonus])
 values ('2008-09-22','Anton','Tarasovich','Pavlov','�O1134184','1008147245',2,15000,400)
insert into [employee]([date_of_employment],[first_name],[middle_name],[last_name],[passport_number],[identity_code],[department_id],[salary],[bonus])
 values ('2017-10-13','Taras','Pavlovich','Antonov','�B1304109','1599147575',2,18000,500)
insert into [employee]([date_of_employment],[first_name],[last_name],[passport_number],[identity_code],[department_id],[salary],[bonus])
 values ('2016-01-12','Mateush','voitovski','PL00141840','0980245245',3,30000,1500)
insert into [employee]([date_of_employment],[first_name],[last_name],[passport_number],[identity_code],[department_id],[salary],[bonus])
 values ('2009-03-14','Pavel','Pshepshecki','PL12541805','1018102275',3,20000,500)
insert into [employee]([date_of_employment],[first_name],[last_name],[passport_number],[identity_code],[department_id],[salary],[bonus])
 values ('2009-08-29','Jhon','Dow','US12221451','1125167585',4,35000,150)
insert into [employee]([date_of_employment],[first_name],[last_name],[passport_number],[identity_code],[department_id],[salary],[bonus])
 values ('2009-11-14','David','Smith','US00236354','5244548759',4,15000,500)
insert into [employee]([date_of_employment],[first_name],[last_name],[passport_number],[identity_code],[department_id],[salary],[bonus])
 values ('2010-09-13','Jhon','Malkovich','US23528551','0275638574',5,25000,1150)
insert into [employee]([date_of_employment],[first_name],[last_name],[passport_number],[identity_code],[department_id],[salary],[bonus])
 values ('2017-01-20','Edd','Edison','US12158545','1005528545',5,18000,1200)
go
-- ---------------------------------- add boss id to department
update department
set [boss_id] = 1
where id = 1
update department
set [boss_id] = 3
where id = 2
update department
set [boss_id] = 6
where id = 3
update department
set [boss_id] = 7
where id = 4
update department
set [boss_id] = 9
where id = 5
go
use [master]