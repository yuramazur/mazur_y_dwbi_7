use [Y_M_module_3]
go
select * from employee
select * from department
go
-- ----------- trrigers work check ----
update [employee] 
set [salary] = [salary]*0.5
where [department_id] in (1,2,4)
go
update [department]
set [city] = 'Lviv'
where [country] like 'Ukrain'
go
select * from employee
select * from department
-- -----------------------------------
-- ----------- view "with check option" work check ----

select * from [vw_employee_lviv]

go

update [vw_employee_lviv] 
set [department_city] = 'Kiev'
where id = 1

go

begin transaction 

update [department]
set [city] = 'Lviv'
where [country] like 'Ukraine'

update [vw_employee_lviv]
set [department_city] = 'Kiev'
where [country] like 'Ukraine'

update [vw_employee_lviv]
set [department_name] = 'Alfa'
where [country] like 'Ukraine'

select * from [vw_employee_lviv]
select * from [department]

rollback

go
use [master]   