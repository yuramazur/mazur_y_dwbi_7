use [labor_sql]
go
------------- 1  -------------

select   [maker],[type] 
from     [product]
where    [type] like 'laptop'
order by [maker]

------------- 2  -------------

select   [model],[screen],[ram],[price]
from     [laptop]
where    [price] > 1000
order by [ram] asc, [price] desc

------------- 3  -------------

select   * 
from     [printer]
where    [color] = 'y'
order by [price] desc

------------- 4  ------------- -DONE-

select   [model],[speed],[hd],[cd],[price]
from     [pc]
where    ([cd] = '12x' or [cd] = '24x') and [price] < 600
order by [price] desc

------------- 5  -------------

select   [name], [class]
from     [ships]
where    [name] like [class]
order by [name] 

------------- 6  -------------

select   *
from     [pc]
where    [speed] >= 500 and [price] < 800
order by [price] desc 

------------- 7  -------------

select   *
from     [printer]
where    [type] not like 'matrix' and [price] < 300
order by [type] desc

------------- 8  -------------

select   [model],[speed]
from     [pc]
where    [price] between 400 and 600
order by [hd] 

------------- 9  -------------

select   [model],[speed],[hd],[price]
from     [laptop]
where    [screen] >= 12
order by [price] desc

------------- 10 -------------

select   [model],[type],[price]
from     [printer]
where    [price] < 300 
order by [type] desc

------------- 11 -------------

select   [model],[price],[ram]
from     [laptop]
where    [ram] = 64
order by [screen]

------------- 12 -------------

select   [model],[ram],[price]
from     [pc]
where    [ram] > 64
order by [hd]

------------- 13 -------------

select   [model],[speed],[price]
from     [pc]
where    [speed] between 500 and 750
order by [hd] desc
 
------------- 14 -------------

select   *
from     [outcome_o]
where    [out] > 2000
order by [date] desc

------------- 15 -------------

select   *
from     [income_o]
where    [inc] between 5000 and 10000
order by [inc]

------------- 16 -------------

select   * 
from     [income]
where    [point] = 1
order by [inc]

------------- 17 -------------

select   *
from     [outcome]
where    [point] = 2
order by [out]

------------- 18 -------------

select   *
from     [classes]
where    [country] like 'Japan'
order by [type] desc

------------- 19 -------------

select   [name],[launched]
from     [ships]
where    [launched] between 1920 and 1942
order by [launched] desc

------------- 20 -------------

select   [ship],[battle],[result]
from     [outcomes]
where    [battle] like 'Guadalcanal' and  not [result] = 'sunk'
order by [ship] desc

------------- 21 -------------

select   [ship],[battle],[result]
from     [outcomes]
where    [result] = 'sunk'
order by [ship] desc

------------- 22 -------------

select   [class],[displacement]
from     [classes]
where    [displacement] > = 40000
order by [type]

------------- 23 -------------

select   [trip_no],[town_from],[town_to]
from     [trip]
where    [town_from] = 'London' or [town_to] = 'London'
order by [time_out]

------------- 24 -------------

select   [trip_no],[plane],[town_from],[town_to]
from     [trip]
where    [plane] like 'TU-134'
order by [time_out]

------------- 25 -------------

select   [trip_no],[plane],[town_from],[town_to]
from     [trip]
where    [plane] not like 'IL-86'
order by [plane]

------------- 26 -------------

select   [trip_no],[town_from],[town_to]
from     [trip]
where    [town_from] not like 'Rostov' and [town_to] not like 'Rostov'
order by [plane]

------------- 27 -------------

select   *
from     [pc]
where    [model] like '%1%1%'

------------- 28 -------------

select   *
from     [outcome]
where    MONTH([date]) = 3
 
------------- 29 -------------

select   *
from     [outcome_o]
where    DAY([date]) = 14

------------- 30 -------------

select   *   
from     [ships]
where    [name] like 'W%n'

------------- 31 -------------

select   *   
from     [ships]
where    LEN([name])-LEN(REPLACE([name],'e','')) = 2

------------- 32 ------------- -done-

select   [name],[launched]
from     [ships]
where    [name] not like '%a'

------------- 33 -------------

select distinct [battle]
from            [outcomes]
where           LEN([battle])-LEN(REPLACE([battle],' ','')) = 1 and [battle] not like '%c'

------------- 34 ------------- -DONE-

select   *
from     [trip]
where    (CONVERT(decimal,DATEPART(HOUR, [time_out])) + 
         (CONVERT(decimal,DATEPART(MINUTE,[time_out]))/60)) 
          between 12 and 17

------------- 35 ------------- -DONE- 

select   *
from     [trip]
where    (CONVERT(decimal,DATEPART(HOUR, [time_in])) + 
         (CONVERT(decimal,DATEPART(MINUTE,[time_in]))/60))  
		  between 17 and 23

------------- 36 ------------- -DONE-

select   *
from     [trip]
where    (DATEPART(HOUR, [time_in]) between 21 and 23) 
          or
         ((CONVERT(decimal,DATEPART(HOUR, [time_in])) + 
         (CONVERT(decimal,DATEPART(MINUTE,[time_in]))/60))  
		  between  0 and 10)

------------- 37 -------------

select   CONVERT(DATE,[date],101) as [date]
from     [pass_in_trip]
where    [place] like '1_' 

------------- 38 -------------

select   CONVERT(DATE,[date],101) as [date]
from     [pass_in_trip]
where    [place] like '_c' 

------------- 39 -------------

select   p.[surname]
from     (select LTRIM(REPLACE([name],(select top 1 value from STRING_SPLIT([name],' ')),'')) as [surname]
         from   [passenger]) p
where    p.[surname] like 'c%'

------------- 40 -------------

select   p.[surname]
from     (select LTRIM(REPLACE([name],(select top 1 value from STRING_SPLIT([name],' ')),'')) as [surname]
         from   [passenger]) p
where    p.[surname] not like 'j%'

------------- 41 -------------

select   CONCAT('������� ���� = ', AVG([price])) as [avg_price]
from     [laptop]

------------- 42 -------------

select   CONCAT('���: ',[code])code, 
         CONCAT('������: ',[model])model, 
		 CONCAT('������� ���������: ',[speed])speed, 
		 CONCAT('���������� ������: ',[ram],' Mb')ram, 
		 CONCAT('�������� ���� : ',[hd],' Gb')hd,
		 CONCAT('���� : ',[price],' $')price
from     [pc]

------------- 43 -------------

select   REPLACE(CONVERT(date,[date],101),'-','.') as [date]
from     [income]

------------- 44 -------------

select   [ship],[battle], CASE when [result] = 'sunk'    then '����������'
                               when [result] = 'damaged' then '�����������'
							   when [result] = 'OK'      then '��� ����������'
                           END as   [result]
from     [outcomes]

------------- 45 ------------- 

select   '���: ' + LEFT([place],1) + ' ����: ' + RIGHT(RTRIM([place]),1) 
from     [pass_in_trip]

------------- 46 -------------

select   [trip_no],[id_comp],[plane],
         ('from '+TRIM([town_from])+' to '+TRIM([town_to])) as [rout],
		 [time_out],[time_in]
from     [trip]

------------- 47 -------------

select   LEFT([trip_no],1)   + RIGHT(RTRIM([trip_no]),1) +
         LEFT([id_comp],1)   + RIGHT(RTRIM([id_comp]),1) +
		 LEFT([plane],1)     + RIGHT(RTRIM([plane]),1) +
		 LEFT([town_from],1) + RIGHT(RTRIM([town_from]),1) +
		 LEFT([town_to],1)   + RIGHT(RTRIM([town_to]),1) +
		 LEFT(CONVERT(varchar,[time_out],114),1)  + RIGHT(CONVERT(varchar,[time_out],114),1) +
		 LEFT(CONVERT(varchar,[time_in],114),1)  + RIGHT(CONVERT(varchar,[time_in],114),1) as [combine]
from     [trip]

------------- 48 -------------

select   [maker],COUNT(*) as [models]
from     [product]
where    [type] = 'pc'
group by [maker]
having   count(*) > 1

------------- 49 -------------

select   t.[town] , COUNT(*) as [races]
from     (select [town_to]   as [town]  from [trip]
          union all
		  select [town_from] as [town] from [trip]) as t
group by [town]

------------- 50 -------------

select   [type] ,COUNT(*) as [amount]
from     [printer]
group by [type]

------------- 51 ------------- -DONE-
 
 select   NULL as [model], [cd],COUNT(*) as [amount_model], NULL as [amount_cd]
 from     [pc]
 group by cube([cd])
 union all 
 select   [model], NULL, NULL, COUNT(*)
 from     [pc]
 group by cube([model])

------------- 52 -------------

select   [trip_no],[plane],[town_from],[town_to],
          CONVERT(varchar,DATEADD(SS,(DATEDIFF(SS,[time_out],[time_in])),0),108) as [time_in_air]
from     [trip]

------------- 53 -------------

select   [point], [date], SUM(out) as [sum],null as [max_sum],null as [min_sum]
from     [outcome]
group by grouping sets(([point], [date]),
                        [point])
union all
select   t.[point],NULL,NULL, max(t.[sum]), min(t.sum)
from     (select   [point],[date], SUM(out) as [sum]
          from     [outcome]
          group by[point], [date]) as t
group by t.[point]
union all
select   NULL,NULL,NULL, max(t.[sum]), min(t.[sum])
from     (select   [point], SUM(out) as [sum]
          from     [outcome]
          group by [point]) as t

------------- 54 -------------

select   [trip_no], LEFT([place],1) as [row], COUNT(*) as [pasengers]
from     [pass_in_trip]   
group by [trip_no], LEFT([place],1)

------------- 55 ------------- -DONE-
-- ���� ������ �� ���� �������(����� ����� � ������� name)

select   COUNT(*) as [amount]
from     (select p.[surname] 
          from   (select LTRIM(REPLACE([name],(select top 1 value from STRING_SPLIT([name],' ')),'')) as [surname]
                            from   [passenger]) as p  
	      where  p.[surname] like 'S%' or p.[surname] like 'B%' or p.[surname] like 'A%') as t  
