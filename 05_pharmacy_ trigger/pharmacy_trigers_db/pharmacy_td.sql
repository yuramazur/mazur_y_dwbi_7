use [triger_fk_cursor]
go
----PHARMACY SIDE -----
create or alter trigger [pharmacy_insert_update_delete] 
on [pharmacy]
after insert, update, delete
as
BEGIN
 IF @@ROWCOUNT = 0 return 
   declare @icount int = (select count(*) from inserted)
   declare @dcount int = (select count(*) from deleted)
   declare @type varchar(1)
   set @type = 
 case 
	when @icount > 0 and @dcount > 0 then 'U'  
	when @icount = 0 and @dcount > 0 then 'D'  
	when @icount > 0 and @dcount = 0 then 'I'  
 end
IF @type = 'U' or @type = 'I'
   begin 
   declare @idins int = (  select count(*) 
                        from inserted
						where [street] not in (select * from [street]))
						
         IF @idins > 0
		 begin 
            ROLLBACK
			PRINT 'incorect foreign key1'
         end
    end
    IF @type = 'U'
	   begin
------------- NO ACTION ----------------------
       if (select count(*) from [employee] where [pharmacy_id] in (select [id] from deleted)) > 0
       rollback
	   print 'The statement has been terminated2'
--------------------------------------------
--------------- CASCADE ----------------------
       --update [employee]
       --set [pharmacy_id] = (select [id] from inserted)
       --where [pharmacy_id] in (select [id] from deleted)
--------------------------------------------
--------------- SET NULL ----------------------
       --update [employee]
       --set [pharmacy_id] = NULL
       --where [pharmacy_id] in (select [id] from deleted)
----------------------------------------------
--------------- SET DEFAULT --------------------
       --update [employee]
       --set [pharmacy_id] = DEFAULT
       --where [pharmacy_id] in (select [id] from deleted)
------------------------------------------------
	   end
IF @type ='D'
   begin
------------- NO ACTION ----------------------
       if (select count(*) from [employee] where [pharmacy_id] in (select [id] from deleted)) > 0
       print 'The statement has been terminated3'
       rollback
--------------------------------------------
--------------- CASCADE ----------------------
       --delete [employee]
       --where [pharmacy_id] in (select [id] from deleted)
--------------------------------------------
--------------- SET NULL ----------------------
       --update [employee]
       --set [pharmacy_id] = NULL
       --where [pharmacy_id] in (select [id] from deleted)
----------------------------------------------
--------------- SET DEFAULT --------------------
       --update [employee]
       --set [pharmacy_id] = DEFAULT
       --where [pharmacy_id] in (select [id] from deleted)
------------------------------------------------
   end
END
go 
use [master]