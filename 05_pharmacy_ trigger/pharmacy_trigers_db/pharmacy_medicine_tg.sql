use [triger_fk_cursor]
go
create or alter trigger [pharmacy_medicine_inset]
on [pharmacy_medicine]
after insert, update
as
BEGIN
 IF @@ROWCOUNT = 0 RETURN
 declare @pharmacy int = (select count(*) from inserted where [pharmacy_id] not in(select [id] from [pharmacy] ))
 declare @medicine int = (select count(*) from inserted where [medicine_id] not in(select [id] from [medicine] ))
  if @pharmacy > 0 or @medicine > 0
  begin
  ROLLBACK
  PRINT 'incorect foreign key'
  end
END
go
use [master]