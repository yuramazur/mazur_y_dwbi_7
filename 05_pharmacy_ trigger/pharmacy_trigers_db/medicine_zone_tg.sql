use [triger_fk_cursor]
go
create or alter trigger [medicine_zone_inset_update]
on [medicine_zone]
after insert, update
as
BEGIN
 IF @@ROWCOUNT = 0 RETURN
 declare @zone int = (select count(*) from inserted where [zone_id] not in(select [id] from [zone]))
 declare @medicine int = (select count(*) from inserted where [medicine_id] not in(select [id] from [medicine] ))
  if @zone > 0 or @medicine > 0
  begin
  ROLLBACK
  PRINT 'incorect foreign key'
  end
END
go
use [master]