use [triger_fk_cursor]
go
----EMPLOYEE SIDE -----
create or alter trigger [employee_insert_update] 
on [employee]
after insert, update
as
BEGIN
IF @@ROWCOUNT = 0 RETURN 
declare @post int = (    select count([post]) 
                         from inserted
						 where [post] not in (select * from post))
					 
declare @pharmacy int = (select count([pharmacy_id]) 
                         from inserted
						 where [pharmacy_id] not in (select [id] from [pharmacy]))
 if @post > 0 or @pharmacy > 0 
 begin
 PRINT 'incorect foreign key'
 ROLLBACK
 end
 END
go
use [master]