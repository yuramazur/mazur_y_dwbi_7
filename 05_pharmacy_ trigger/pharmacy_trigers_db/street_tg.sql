use [triger_fk_cursor]
go
create or alter trigger [street_update_delete]
on [street]
after delete, update
as
IF @@ROWCOUNT = 0 RETURN
BEGIN
declare @del int = (select count(*) from deleted)
declare @up int = (select count(*) from inserted)
--------------- NO ACTION ----------------------
--  if @del > 0
--  if (select count(*) from [pharmacy] where [street] in (select * from deleted)) > 0
--  print 'The statement has been terminated.'
--  rollback
------------------------------------------------
------------- CASCADE ----------------------
if @up = 0 
delete [pharmacy] 
where [street] in (select * from deleted)
else
update [pharmacy]
set [street] = (select* from inserted)
where [street] in (select * from deleted)
--------------------------------------------
--------------- SET NULL ----------------------
--if @del > 0 
--update [pharmacy]
--set [street] = NULL
--where [street] in (select * from deleted)
----------------------------------------------
--------------- SET DEFAULT --------------------
--if @del > 0 
--update [pharmacy]
--set [street] = DEFAULT
--where [street] in (select * from deleted)
------------------------------------------------
END
go
use [master] 
