USE [master]
GO
DROP DATABASE IF EXISTS [triger_fk_cursor]
CREATE DATABASE [triger_fk_cursor]
GO
USE [triger_fk_cursor]
GO

CREATE TABLE [employee](
    id                 INT               IDENTITY(1,1),
    surname            VARCHAR(30)       NOT NULL,
    name               CHAR(30)          NOT NULL,
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
    experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15)       NOT NULL,
    pharmacy_id        INT,
    PRIMARY KEY (id)
)

CREATE TABLE [medicine](
    id               INT            IDENTITY(1,1),
    name             VARCHAR(30)    NOT NULL,
    ministry_code    CHAR(10),
    recipe           BIT,
    narcotic         BIT,
    psychotropic     BIT,
    PRIMARY KEY (id)
)

CREATE TABLE [medicine_zone](
    medicine_id    INT    NOT NULL,
    zone_id        INT    NOT NULL,
    PRIMARY KEY (medicine_id, zone_id)
)

CREATE TABLE [pharmacy](
    id                 INT            IDENTITY(1,1),
    name               VARCHAR(15)    NOT NULL,
    building_number    VARCHAR(10),
    www                VARCHAR(40),
    work_time          TIME,
    saturday           BIT,
    sunday             BIT,
    street             VARCHAR(25),
    PRIMARY KEY (id)
)

CREATE TABLE [pharmacy_medicine](
    pharmacy_id    INT    NOT NULL,
    medicine_id    INT    NOT NULL,
    PRIMARY KEY (pharmacy_id, medicine_id)
)

CREATE TABLE [post](
    post    VARCHAR(15)    NOT NULL,
    PRIMARY KEY (post)
)

CREATE TABLE [street](
    street    VARCHAR(25)    NOT NULL,
    PRIMARY KEY (street)
)

CREATE TABLE [zone](
    id      INT            IDENTITY(1,1),
    name    VARCHAR(25)    NOT NULL,
    PRIMARY KEY (id)
)
go
use [master]


