use triger_fk_cursor
go
create or alter trigger [medicine_insert_update_delete]
on[medicine]
after insert, update, delete 
as
BEGIN
IF @@ROWCOUNT = 0 RETURN
 begin
declare @del int = (select count(*) from deleted)
declare @up int = (select count(*) from inserted)
------------- NO ACTION ----------------------
if @del > 0
begin
  if (select count(*) from [medicine_zone] where [medicine_id] in (select [id] from deleted)) > 0 
      or
     (select count(*) from [pharmacy_medicine] where [medicine_id] in (select [id] from deleted)) > 0
   rollback
   print 'The statement has been terminated.'
end
----------------------------------------------
--------------- CASCADE ----------------------
--if @up = 0 
--delete [medicine_zone] 
--where [medicine_id] in (select [id] from deleted)
--delete [pharmacy_medicine]
--where [medicine_id] in (select [id] from deleted)
--else
--update [medicine_zone]
--set [medicine_id] = (select [id] from inserted)
--where [medicine_id] in (select [id] from deleted)
--update [pharmacy_medicine]
--set [medicine_id] = (select [id] from inserted)
--where [medicine_id] in (select [id] from deleted)
----------------------------------------------
--------------- SET NULL ----------------------
--if @del > 0 
--update [medicine_zone]
--set [medicine_id] = NULL
--where [medicine_id] in (select [id] from deleted)
--update [pharmacy_medicine]
--set [medicine_id] = NULL
--where [medicine_id] in (select [id] from deleted)
----------------------------------------------
--------------- SET DEFAULT --------------------
--if @del > 0 
--update [medicine_zone]
--set [medicine_id] = DEFAULT
--where [medicine_id] in (select [id] from deleted)
--update [pharmacy_medicine]
--set [medicine_id] = DEFAULT
--where [medicine_id] in (select [id] from deleted)
------------------------------------------------
 end
END

go
use [master] 