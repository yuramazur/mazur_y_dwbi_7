use [triger_fk_cursor]
go
---- POST SIDE -----
create or alter trigger [post_update_delete]
on [post]
after delete, update
as
IF @@ROWCOUNT = 0 RETURN
BEGIN
declare @del int = (select count(*) from deleted)
declare @up int = (select count(*) from inserted)
--------------- NO ACTION ----------------------
--if @del > 0
--if (select count(*) from [employee] where [post] in (select * from deleted)) > 0
--print 'The statement has been terminated.'
--rollback
----------------------------------------------
------------- CASCADE ----------------------
if @up = 0 
delete [employee] 
where [post] in (select * from deleted)
else
update [employee]
set [post] = (select* from inserted)
where [post] in (select * from deleted)
--------------------------------------------
--------------- SET NULL ----------------------
--if @del > 0 
--update [employee]
--set [post] = NULL
--where [post] in (select * from deleted)
----------------------------------------------
--------------- SET DEFAULT --------------------
--if @del > 0 
--update [employee]
--set [post] = DEFAULT
--where [post] in (select * from deleted)
------------------------------------------------
END
go
use [master] 