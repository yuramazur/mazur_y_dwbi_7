use [triger_fk_cursor]

go 

insert into [zone]([name]) values ('heart'),
                                  ('head'),
								  ('stomach'),
								  ('liver'),
								  ('vessels'),
								  ('pancreas'),
								  ('kidneys'),
								  ('eyes'),
								  ('nouse'),
								  ('teeth')
go
insert into [street]([street]) values('Banderu'),
                                  ('Shuhevucha'),
								  ('Konovalcia'),
								  ('Shvachku'),
								  ('Kruvonosa'),
								  ('Sirka'),
								  ('Geroiiv UPA'),
								  ('Sichovuh Strilciv'),
								  ('Kozacka'),
								  ('Konotopska')
go
insert into [post]([post]) values ('pharmacist'),
                                  ('manager'),
								  ('worker')
go
insert into [pharmacy]([name],[building_number],[www],[street])values ('one','123','one.pharmacy.com','Banderu'),
                                                                      ('two','14h','two.pharmacy.com','Shuhevucha'),
																	  ('three','1b','three.pharmacy.com','Shuhevucha'),
																	  ('four','25','four.pharmacy.com','Shvachku'),
																	  ('five','16','five.pharmacy.com','Kruvonosa'),
																	  ('six','14','six.pharmacy.com','Sirka'),
																	  ('seven','46','seven.pharmacy.com','Geroiiv UPA'),
																	  ('eight','58','eight.pharmacy.com','Sichovuh Strilciv'),
																	  ('nine','10','nine.pharmacy.com','Kozacka'),
																	  ('ten','11','ten.pharmacy.com','Konotopska')
go
insert into [employee]([surname],[name],[midle_name],[identity_number],[passport],[experience],[birthday],[post],[pharmacy_id])values('Antonov','Anton','Antonovich','KB10764582','5326987456',12,'1985-05-16','manager',1),
                                                                                                                                     ('Bogdanova','Bogdana','Bogdanivna','KC10788582','5311987456',15,'1975-10-25','pharmacist',1),
																																	 ('Pavlov','Pavlo','Pavlov','KC10788582','0221987412',9,'1985-05-16','worker',1),
																																	 ('Pulupiv','Pulup','Pulupovich','KN12758951','0225256251',2,'1990-08-26','manager',2),
																																	 ('Bogdaniv','Anton','Pulipovich','KO12758951','1225223541',25,'1965-05-10','pharmacist',3),
																																	 ('Soloma','Solomija','Bogdanivna','KD15286561','1225223501',11,'1985-12-01','manager',3),
																																	 ('Salovski','Volodumur','Igorovich','KB55881256','1245223541',2,'1995-01-16','worker',4),
																																	 ('Domin','Dmutro','Petrovich','KB01095255','1002399541',12,'1985-12-16','manager',4),
																																	 ('Kozakov','Oleg','Igorovich','KD15286599','1002028741',2,'1989-05-16','worker',5),
																																	 ('Hochu','Yurii','Pavlovich','VD00286599','1001199541',12,'1985-01-06','manager',5),
																																	 ('Spatu','Solomija','Antonivna','VK55266599','1252399251',2,'1998-09-05','manager',6),
																																	 ('Duge','Viola','Fedorivna','VM05236599','1992505251',9,'1990-03-09','pharmacist',6),
																																	 ('Navit','Pavlo','Pavlovich','KL15266599','5552399541',8,'1988-12-16','manager',7),
																																	 ('Neznaju','Olga','Petrivna','OP01527414','0001252541',2,'1996-05-16','worker',7),
																																	 ('Sho','Semen','Jaroslavovich','KK1188514','1111252541',1,'1987-10-06','manager',8),
																																	 ('Robutu','Igor','Olegovich','HH1252024','000101111',15,'1985-01-12','manager',9),
																																	 ('Ottacko','Dmutro','Romanovich','VH12001155','891101651',12,'1975-07-14','manager',10)
go
insert into [medicine]([name],[ministry_code]) values ('Sercesoplin','OH-123-00'),
                                                     ('Nurcoshlunkin','OH-123-01'),
													 ('Golovonebolin','OH-123-02'),
													 ('Venoochin','OH-123-03'),
													 ('Zubonegolin','OH-123-04'),
													 ('Pishlunkin','OH-123-05'),
													 ('Sercestucin','OH-123-06'),
													 ('Sercenebolin','OH-123-07'),
								                     ('Soplinetechin','OH-123-08'),
													 ('Okonekrasnin','OH-123-09'),
													 ('Zubobilin','OH-123-10')
													 
go
insert into [medicine_zone](medicine_id,zone_id) values (1,1),(2,9),(2,3),(1,7),
                                                         (3,2),(3,10),(4,5),(4,7),
														 (5,2),(5,10),(6,3),(6,6),
														 (7,1),(7,5),(8,1),(8,5),
														 (9,9),(9,7),(10,7),(10,9),
														 (11,10)
go
insert into [pharmacy_medicine](pharmacy_id,medicine_id) values (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),
                                                                (2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(2,8),(2,9),(2,10),(2,11),
																(3,1),(3,2),(3,3),(3,4),(3,5),(3,6),(3,7),(3,8),(3,9),(3,10),(3,11),
																(4,1),(4,2),(4,3),(4,4),(4,5),(4,6),(4,7),(4,8),(4,9),(4,10),(4,11),
																(5,1),(5,2),(5,3),(5,4),(5,5),(5,6),(5,7),(5,8),(5,9),(5,10),(5,11),
																(6,1),(6,2),(6,3),(6,4),(6,5),(6,6),(6,7),(6,8),(6,9),(6,10),(6,11),
																(7,1),(7,2),(7,3),(7,4),(7,5),(7,6),(7,7),(7,8),(7,9),(7,10),(7,11),
																(8,1),(8,2),(8,3),(8,4),(8,5),(8,6),(8,7),(8,8),(8,9),(8,10),(8,11),
																(9,1),(9,2),(9,3),(9,4),(9,5),(9,6),(9,7),(9,8),(9,9),(9,10),(9,11),
																(10,1),(10,2),(10,3),(10,4),(10,5),(10,6),(10,7),(10,8),(10,9),(10,10),(10,11)
go
use [master]																						  								  								  								  								  								  								  								  								  								  								  