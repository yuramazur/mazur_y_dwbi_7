use [triger_fk_cursor]
go
---- POST SIDE -----
create or alter trigger [zone_update_delete]
on [zone]
after delete, update
as
IF @@ROWCOUNT = 0 RETURN
BEGIN
declare @del int = (select count(*) from deleted)
declare @up int = (select count(*) from inserted)

------------- NO ACTION ----------------------
if @del > 0
if (select count(*) from [medicine_zone] where [zone_id] in (select [id] from deleted)) > 0
print 'The statement has been terminated.'
rollback
--------------------------------------------
--------------- CASCADE ----------------------
--if @up = 0 
--delete [medisine_zone] 
--where [zone] in (select * from deleted)
--else
--update [medisine_zone]
--set [zone] = (select* from inserted)
--where [zone] in (select * from deleted)
----------------------------------------------
--------------- SET NULL ----------------------
--if @del > 0 
--update [medisine_zone]
--set [zone] = NULL
--where [zone] in (select * from deleted)
----------------------------------------------
--------------- SET DEFAULT --------------------
--if @del > 0 
--update [medisine_zone]
--set [zone] = DEFAULT
--where [zone] in (select * from deleted)
------------------------------------------------
END
go
use [master] 
