use [triger_fk_cursor]
go

create or alter trigger [employee_pattern] 
on [employee]
after insert, update
as
BEGIN
IF @@ROWCOUNT = 0 RETURN 
declare @pattern int = (select count(*) from inserted where [identity_number] like '%[0][0]')

 if @pattern > 0 
 begin
 ROLLBACK
 PRINT 'identity number cannot end with "00"'
 end
 END
go
use [master]