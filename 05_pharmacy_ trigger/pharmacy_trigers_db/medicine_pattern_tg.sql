use triger_fk_cursor
go
create or alter trigger [medicine_pattern]
on[medicine]
after insert, update
as
BEGIN
IF @@ROWCOUNT = 0 RETURN
begin
declare @pattern int =(select count(*) from inserted where [ministry_code] like '[MP]%') 
	                 +(select count(*) from inserted where [ministry_code] like '[A-Z][MP]%')
	                 +(select count(*) from inserted where [ministry_code] not like '[A-Z][A-Z][-][0-9][0-9][0-9][-][0-9][0-9]')
if @pattern > 0 
begin
rollback
print 'wrong format ministry code'
end
end
END
go
use [master]