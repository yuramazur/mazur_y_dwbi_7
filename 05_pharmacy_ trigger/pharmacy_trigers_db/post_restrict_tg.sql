use [triger_fk_cursor]
go
---- POST SIDE -----
create or alter trigger [post_restrict]
on [post]
after delete, update
as
IF @@ROWCOUNT = 0 RETURN
ELSE
BEGIN
ROLLBACK
PRINT 'any changes restricted'
END
go
use [master] 