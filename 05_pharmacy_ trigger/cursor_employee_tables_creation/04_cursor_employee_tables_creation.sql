use [triger_fk_cursor]
go
drop schema if exists [name_tabs]
go
create schema [name_tabs] 
go
declare @surname varchar(30) 
declare @name char(30)
declare @middle_name varchar(30)  
declare @count int
declare [table_creation] cursor for 
                         select distinct [surname],[name],[midle_name] from [employee]
declare @create nvarchar(max) = 'create table name_tabs.'
       open [table_creation]
	        fetch next from [table_creation] into @surname, @name ,@middle_name
			while  @@FETCH_STATUS = 0
			BEGIN 
			     set @count = ROUND(((RAND()*10)+1),0) 
			     declare @sql nvarchar(max) = ''
			     declare @row nvarchar(max) = ''
			             while @count > 0
				         begin
						       if (@count-1) = 0
							   begin
				                   set @row = concat(@row,'row',@count,' int')
							   end
				               else
							   begin
				                   set @row = concat(@row,'row',@count,' int,')
							   end
							   set @count = @count - 1
				         end
			set @sql = concat(@create,RTRIM(@surname),RTRIM(@name),RTRIM(@middle_name),'(',@row,')')
			EXECUTE sp_executesql @sql
			PRINT @sql
			fetch next from [table_creation] into @surname, @name ,@middle_name
			END
       close [table_creation]
       deallocate [table_creation] 
go