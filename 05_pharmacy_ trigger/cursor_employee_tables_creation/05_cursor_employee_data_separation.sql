use [triger_fk_cursor]
go
declare @id int
declare @surname varchar(30) 
declare @name char(30)
declare @middle_name varchar(30)
declare @identity_number char(10)  
declare @pasport char(10)
declare @experience decimal(10,1)
declare @birthday date
declare @post varchar(15)
declare @pharmacy_id int
declare @datetime datetime
declare @count int
declare @create nvarchar(max) = 'create table name_tabs.employee'
declare @sql nvarchar(max) = '(
	id                 INT               ,
    surname            VARCHAR(30)       NOT NULL,
    name               CHAR(30)          NOT NULL,
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
    experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15)       NOT NULL,
    pharmacy_id        INT,
     time_stamp         DATETIME
	)' 
declare @table1 nvarchar(max)
declare @table2 nvarchar(max)
declare [table_copy] cursor for 
                         select distinct * from [employee]

       open [table_copy]
    drop table if exists [name_tabs].[employee_left]
    drop table if exists [name_tabs].[employee_right]
	set @table1 = concat(@create,'_left', @sql)
	set @table2 = concat(@create,'_right', @sql)
	print @table1
	EXECUTE sp_executesql @table1
	EXECUTE sp_executesql @table2
		        fetch next from [table_copy] into @id, @surname, @name, @middle_name, @identity_number, @pasport, @experience, @birthday, @post, @pharmacy_id 
			while  @@FETCH_STATUS = 0
			BEGIN 
			set @datetime = GETDATE()
			     set @count = ROUND(((RAND()*10)+1),0) 
			     if @count < 5 
				 begin
				 insert into [name_tabs].[employee_left] select @id, @surname, @name, @middle_name, @identity_number, @pasport, @experience, @birthday, @post, @pharmacy_id,@datetime 
				 end
				 else
				 begin
				 insert into [name_tabs].[employee_right] select @id, @surname, @name, @middle_name, @identity_number, @pasport, @experience, @birthday, @post, @pharmacy_id,@datetime 
				 end
			fetch next from [table_copy] into @id, @surname, @name, @middle_name, @identity_number, @pasport, @experience, @birthday, @post, @pharmacy_id 
			END
       close [table_copy]
       deallocate [table_copy] 
go
select * from [name_tabs].[employee_left]
select * from [name_tabs].[employee_right]
go 
use master