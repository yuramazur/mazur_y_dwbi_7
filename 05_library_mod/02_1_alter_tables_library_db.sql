use [Library]
go
alter table [Authors]
add 
birthday date null,
book_amount int not null default 0 check (book_amount > = 0),
issue_amount int not null default 0 check(issue_amount >= 0),
total_edition int not null default 0 check(total_edition >= 0)

go
alter table [BooksAuthors]
drop constraint [fk_books_authors_books]
alter table [Books]
drop constraint [pk_books]

go

alter table [Books]
add 
title varchar(30) not null default 'Title',
edition int not null default 1 check(edition >= 1),
published date null,
issue int not null

alter table [Books]
add constraint [pk_books] primary key ([ISBN],[issue])
go

alter table [Publishers]
add
created date not null default '1900-01-01',
country varchar(30) not null default 'USA',
city varchar(30) not null default 'NY',
book_amount int   not null default 0 check (book_amount >= 0),
issue_amount int not null default 0 check (issue_amount >= 0),
total_edition int not null default 0 check (total_edition >= 0)

go

alter table [Authors_log]
add
book_amount_old int null,
issue_amount_old int null,
total_edition_old int null,
book_amount_new int null,
issue_amount_new int null,
total_edition_new int null
go
alter table [BooksAuthors]
add
issue int not null

go
alter table [BooksAuthors]
add 
constraint [fk_books_authors_books] 
foreign key(ISBN,issue)
references [Books]([ISBN],[issue])
on update cascade
on delete no action

go
use [master]