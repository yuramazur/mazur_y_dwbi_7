use [master]
go
 drop database if exists [Library]
 create database [Library]
 on primary
(name ='Main', filename ='D:\Epam\DW_BI\myDB\Main.mdf'),

filegroup [DATA] default
(name ='DATA',filename ='D:\Epam\DW_BI\myDB\DATA.ndf')
go
use [Library]
go
drop sequence if exists [id_author] 
drop sequence if exists [id_publisher]
go 
create sequence [id_author] 
as int
start with 1
increment by 1
create sequence [id_publisher] 
as int
start with 1
increment by 1
go
create table [Authors](
[author_id] int not null default( next value for [id_author]) constraint [pk_authors] primary key,
[name] varchar(30) not null constraint [un_author_name] unique,
[URL] varchar(30) not null constraint [def_author_url] default 'www.author_name.com',
[inserted] datetime not null constraint [def_author_inserted] default GETDATE(),
[inserted_by] varchar(30) not null constraint [def_author_iserted_by] default 'system_user',
[updated] datetime,
[updated_by] varchar(30)
)
go
create table [Books](
[ISBN] varchar(20) not null constraint [pk_books] primary key,
[publisher_id] int not null,
[URL] varchar(30) not null constraint [un_book_url] unique,
[price] money not null default 0,
[inserted] datetime not null constraint [def_books_inserted] default GETDATE(),
[inserted_by] varchar(30) not null constraint [def_books_iserted_by] default 'system_user',
[updated] datetime,
[updated_by] varchar(30),
constraint chk_book_price check([price]>= 0)
)
go
create table [BooksAuthors](
[books_authors_id] int not null default 1 constraint [pk_books_authors] primary key, 
[ISBN] varchar(20) not null,
[author_id] int not null,
[seq_no] int not null default 1,
[inserted] datetime not null constraint [def_books_authors_inserted] default GETDATE(),
[inserted_by] varchar(30) not null constraint [def_books_authors_iserted_by] default 'system_user',
[updated] datetime,
[updated_by] varchar(30),
constraint [un_books_author_isbn_author_id] unique ([ISBN],[author_id]) ,
constraint [chk_books_authors_id] check([books_authors_id]>=1),
constraint[ chk_books_authors_seq_no] check ([seq_no]>=1)
)

go
create table [Publishers](
[publisher_id] int not null default( next value for [id_publisher]) constraint [pk_publisher] primary key,
[name] varchar(30) not null constraint [un_publisher_name] unique,
[URL] varchar(30) not null constraint [def_publisher_url] default 'www.publisher_name.com',
[inserted] datetime not null constraint [def_publisher_inserted] default GETDATE(),
[inserted_by] varchar(30) not null constraint [def_publisher_iserted_by] default 'system_user',
[updated] datetime,
[updated_by] varchar(30)
)
go
create table [Authors_log](
operation_id int not null identity constraint [pk_author_log] primary key, 
[author_id_new] int,
[name_new] varchar(30),
[URL_new] varchar(30),
[author_id_old] int,
[name_old] varchar(30),
[URL_old] varchar(30),
[operation_type] varchar(1) not null constraint [chk_author_log_op_type] check([operation_type] = 'I' or [operation_type] = 'D' or [operation_type] = 'U'),
[operation_datetime] datetime not null constraint [def_authors_log_op_date_time] default GETDATE()
)
go 
alter table [Books]
add 
constraint [fk_books_publisher] 
foreign key (publisher_id) 
references [Publishers](publisher_id)
go
alter table [BooksAuthors]
add 
constraint [fk_books_authors_books] 
foreign key(ISBN)
references [Books]([ISBN])
on update cascade
on delete no action,
constraint [fk_books_authors_authors]
foreign key(author_id)
references [Authors]([author_id])
on update no action
on delete no action 
go
drop trigger if exists [tr_author_update] 
drop trigger if exists [tr_books_update]
drop trigger if exists [tr_books_authors_update]
drop trigger if exists [tr_publisher_update]
drop trigger if exists [tr_populate_authors_log]

---------------------------------------------------------------------------

go
create or alter trigger [tr_books_update]
on [Books]
after update
as
if @@ROWCOUNT = 0 RETURN
begin
declare @date_time datetime = GETDATE(), @user varchar(30) = CURRENT_USER  
update [Books] 
set [updated] = @date_time, [updated_by] = @user
where [ISBN] in (select [ISBN] from inserted)
end

-----------------------------------------------------------------------------

go
create or alter trigger [tr_books_authors_update]
on [BooksAuthors]
after update
as
if @@ROWCOUNT = 0 RETURN
begin
declare @date_time datetime = GETDATE(), @user varchar(30) = CURRENT_USER  
update [BooksAuthors] 
set [updated] = @date_time, [updated_by] = @user
where [books_authors_id] in (select [books_authors_id] from inserted)
end

--------------------------------------------------------------------------------

go
create or alter trigger [tr_publisher_update]
on [Publishers]
after update
as
if @@ROWCOUNT = 0 RETURN
begin
declare @date_time datetime = GETDATE(), @user varchar(30) = CURRENT_USER  
update [Publishers] 
set [updated] = @date_time, [updated_by] = @user
where [publisher_id] in (select [publisher_id] from inserted)
end

----------------------------------------------------------------------------------

go
create or alter trigger [tr_populate_authors_log]
on [Authors]
after insert, update, delete
as
BEGIN
IF @@ROWCOUNT = 0 return 
   declare @icount int = (select count(*) from inserted)
   declare @dcount int = (select count(*) from deleted)
   declare @date_time datetime = GETDATE()
   declare @type varchar(1)
   set @type = 
case 
	when @icount > 0 and @dcount > 0 then 'U'  
	when @icount = 0 and @dcount > 0 then 'D'  
	when @icount > 0 and @dcount = 0 then 'I'  
end 
  
	
	 if @type = 'I'
	begin
	 insert into [Authors_log]([author_id_new],[name_new],[URL_new],[operation_type],[operation_datetime])  
	        (select [author_id],[name],[URL],@type,@date_time from inserted)
	end
	 if @type = 'D'
	begin
	 insert into [Authors_log]([author_id_old],[name_old],[URL_old],[operation_type],[operation_datetime])    
	        (select [author_id],[name],[URL],@type,@date_time from deleted) 
	end
	 if @type = 'U'
	begin
	update [Authors]
	set [updated] = GETDATE(), [updated_by] = CURRENT_USER
	where [author_id] in (select [author_id] from inserted)
	insert into [Authors_log]([author_id_new],[name_new],[URL_new],[author_id_old],[name_old],[URL_old],[operation_type],[operation_datetime])
	select i.[author_id],i.[name],i.[URL],d.[author_id],d.[name],d.[URL],@type,@date_time from inserted i,deleted d
	end
			
	 
END 
go
use [master]