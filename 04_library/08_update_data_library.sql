use [Library]

go 

update [Authors]
set [name] = 'John Ronald Reuel Tolkien'
where [author_id] = 1

update [Authors]
set [name] = 'George Raymond Richard Martin'
where [author_id] = 2

update [Authors]
set [name] = 'Andrzej Sapkowski'
where [author_id] = 3

update [Authors]
set [name] = 'Glen Cook'
where [author_id] = 4

update [Authors]
set [name] = 'Robert Anson Heinlein'
where [author_id] = 5

update [Authors]
set [name] = 'Roger Joseph Zelazny'
where [author_id] = 6

update [Authors]
set [URL] = 'www.classic-lit.com'
where [author_id] in (7,9,13,10)

go

update [Books]
set [price] = [price]*1.22

go

update [BooksAuthors]
set [seq_no] = [seq_no]/2
where [author_id] between 1 and 10

go

update [Publishers]
set [name] = CONCAT([name],' Company')

go 
use [master]