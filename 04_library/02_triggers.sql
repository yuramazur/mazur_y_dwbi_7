use [Library]
go
drop trigger if exists [tr_author_update] 
drop trigger if exists [tr_books_update]
drop trigger if exists [tr_books_authors_update]
drop trigger if exists [tr_publisher_update]
drop trigger if exists [tr_populate_authors_log]
----------------------------------------------------------------------------------

--go
--create or alter trigger [tr_author_update]
--on [Authors]
--after update
--as
--if @@ROWCOUNT = 0 RETURN
--begin
--declare @date_time datetime = GETDATE(), @user varchar(30) = CURRENT_USER  
--update [Authors] 
--set [updated] = @date_time, [updated_by] = @user
--where [author_id] in (select author_id from inserted)
--end

---------------------------------------------------------------------------

go
create or alter trigger [tr_books_update]
on [Books]
after update
as
if @@ROWCOUNT = 0 RETURN
begin
declare @date_time datetime = GETDATE(), @user varchar(30) = CURRENT_USER  
update [Books] 
set [updated] = @date_time, [updated_by] = @user
where [ISBN] in (select [ISBN] from inserted)
end

-----------------------------------------------------------------------------

go
create or alter trigger [tr_books_authors_update]
on [BooksAuthors]
after update
as
if @@ROWCOUNT = 0 RETURN
begin
declare @date_time datetime = GETDATE(), @user varchar(30) = CURRENT_USER  
update [BooksAuthors] 
set [updated] = @date_time, [updated_by] = @user
where [books_authors_id] in (select [books_authors_id] from inserted)
end

--------------------------------------------------------------------------------

go
create or alter trigger [tr_publisher_update]
on [Publishers]
after update
as
if @@ROWCOUNT = 0 RETURN
begin
declare @date_time datetime = GETDATE(), @user varchar(30) = CURRENT_USER  
update [Publishers] 
set [updated] = @date_time, [updated_by] = @user
where [publisher_id] in (select [publisher_id] from inserted)
end

----------------------------------------------------------------------------------

go
create or alter trigger [tr_populate_authors_log]
on [Authors]
after insert, update, delete
as
BEGIN
IF @@ROWCOUNT = 0 return 
   declare @icount int = (select count(*) from inserted)
   declare @dcount int = (select count(*) from deleted)
   declare @date_time datetime = GETDATE()
   declare @type varchar(1)
   set @type = 
case 
	when @icount > 0 and @dcount > 0 then 'U'  
	when @icount = 0 and @dcount > 0 then 'D'  
	when @icount > 0 and @dcount = 0 then 'I'  
end 
  
	
	 if @type = 'I'
	begin
	 insert into [Authors_log]([author_id_new],[name_new],[URL_new],[operation_type],[operation_datetime])  
	        (select [author_id],[name],[URL],@type,@date_time from inserted)
	end
	 if @type = 'D'
	begin
	 insert into [Authors_log]([author_id_old],[name_old],[URL_old],[operation_type],[operation_datetime])    
	        (select [author_id],[name],[URL],@type,@date_time from deleted) 
	end
	 if @type = 'U'
	begin
	update [Authors]
	set [updated] = GETDATE(), [updated_by] = CURRENT_USER
	where [author_id] in (select [author_id] from inserted)
	insert into [Authors_log]([author_id_new],[name_new],[URL_new],[author_id_old],[name_old],[URL_old],[operation_type],[operation_datetime])
	select i.[author_id],i.[name],i.[URL],d.[author_id],d.[name],d.[URL],@type,@date_time from inserted i,deleted d
	end
			
	 
END 
go
use [master]