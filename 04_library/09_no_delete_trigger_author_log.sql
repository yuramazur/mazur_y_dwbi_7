use [Library]
go
create or alter trigger [trg_author_log_gelete]
on[Authors_log]
instead of delete
as
BEGIN
IF @@ROWCOUNT = 0 RETURN
ROLLBACK
PRINT 'YOU CANNOT DELETE VALUES FROM "Library.dbo.Author_log" TABLE!'
END 

go
use [master] 