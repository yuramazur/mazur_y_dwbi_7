use [Library]

go
delete [Authors]
where [author_id] between 15 and 20
 
go 

delete [Books]
where [price] > 13

go

delete [Publishers]
where [publisher_id] < 6

go 

delete [BooksAuthors]
where [ISBN] = NULL or [author_id] = NULL

go
---- this delete dosen't worck because we have not_delete trigger to [Authors_log] table ---- 
delete [Authors_log]
where [operation_type] = 'I';

select * from Authors
go
use [master]