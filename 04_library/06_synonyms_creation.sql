use [Y_Mazur_Library_synonym]
go
create synonym [dbo].[Authors_synonym] for [Library].[dbo].[Authors]
go
create synonym [dbo].[Authors_log_synonym] for [Library].[dbo].[Authors_log]
go
create synonym [dbo].[Books_synonym] for [Library].[dbo].[Books]
go
create synonym [dbo].[BooksAuthors_synonym] for [Library].[dbo].[BooksAuthors]
go
create synonym [dbo].[Publishers_synonym] for [Library].[dbo].[Publishers]
go
use [master]