use [Y_Mazur_Library_view]
go
create or alter view [Authors_view](
[author_id], 
[name],
[URL], 
[inserted],
[inserted_by],
[updated] ,
[updated_by] 
)
as 
select * from [Library].[dbo].[Authors]
go
create or alter view [Authors_log_view]
as
select * from [Library].[dbo].[Authors_log]
go
create or alter view [Books_view]
as
select * from [Library].[dbo].[Books]
go
create or alter view [BooksAuthors_view]
as
select * from [Library].[dbo].[BooksAuthors]
go
create or alter view [Publisher_view]as
select * from [Library].[dbo].[Publishers]
go
use [master]