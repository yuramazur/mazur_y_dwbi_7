use Library

insert into [Authors]([name],[URL]) values('G.R.R Martin', 'www.grrmartin.com'),
                                          ('J.R.R. Tolkien', 'www.jrrtolkien.com'),
										  ('A. Sapkovski', 'www.witcher.com')
select * from [Authors]

select * from [Authors_log]

update [Authors]
set [name] = 'Jhon Ronald Ruel Tolkien', author_id = 6
where [author_id] = 2

update [Authors]
set [name] = 'An Sapkovski'

where [author_id] = 3

delete [Authors] 
where [author_id] = 3