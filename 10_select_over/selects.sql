use labor_sql
go

--------------------------------------- 1 -------------------------------------

select SUM(num) OVER (partition by [num] 
                      order by t.[id_comp],t.[trip_no]
                      rows unbounded preceding) as num, t.trip_no, t.id_comp
from   (select 1 as [num], [trip_no], [id_comp]
        from   [trip]) as t

--------------------------------------- 2 -------------------------------------

select SUM(num) OVER (partition by t.[id_comp]
                      order by t.[id_comp],t.[trip_no]
                      rows unbounded preceding) as num, t.trip_no, t.id_comp
from   (select 1 as [num], [trip_no], [id_comp]
        from   [trip]) as t

--------------------------------------- 3 -------------------------------------

select  [model],[color],[type],[price]
from    (select *,MIN([price]) OVER ( partition by [type]
                                      order     by [type],[price]
								    ) as [min_price]
         from   [printer]) as p
where    [price] = [min_price]

--------------------------------------- 4 -------------------------------------

select   m.[maker]
from    (select [maker],[model],[type], 1 as amount
         from   [product]
         where  [type] like 'pc') as m
group by m.[maker]
having   SUM(m.[amount]) > 2

--------------- OR ------------------------------------------------------------

select     pc.[maker]
from      (select    [maker], SUM(p.[amount]) OVER ( partition by [type],[maker]
                                                     order     by [type],[maker],[model]) as [amount]
           from     (select *, 1 as [amount] 
                     from [product]) as p
           where     [type] like 'pc') pc
where      pc.[amount] > 2

--------------------------------------- 5 -------------------------------------

;WITH price_num_CTE ([price], [num]) AS (
select   p.[price], SUM(p.[num]) OVER (partition by [num]
                                       order     by [price]
									   rows unbounded preceding ) as [num]
          from     (select distinct [price], 1 as [num]
                    from   [pc]) as p 
)
select [price]
from   price_num_CTE  
where  [num] = (select MAX([num])-1 from price_num_CTE)

--------------------------------------- 6 -------------------------------------

select    *, NTILE(3) OVER (order by (select LTRIM(REPLACE([name],(select top 1 value from STRING_SPLIT([name],' ')),'')))
                            ) as [group]
from       [passenger]

--------------------------------------- 7 -------------------------------------
--select CEILING(CONVERT(decimal(6,4),(COUNT(*)))/3) as [num] from pc

declare @row_total int  = (select COUNT(*) from [pc])
declare @page_total int = (select  CEILING(CONVERT(decimal(6,4),(COUNT(*)))/3) from [pc])
select [code],[model],[speed],[ram],[hd],[cd],[price], 
       SUM([num]) OVER (partition by [num]
                         order by t.[price] desc
                         rows unbounded preceding) as [id],
	  @row_total as [row_total],
	  NTILE(@page_total) OVER ( order by [price] desc ) as [page_num],
	  @page_total as [page_total]
from       (select *, 1 as [num]
		    from [pc]) as t

--------------------------------------- 8 -------------------------------------

;WITH union_CTE    as (
         select [point],[date],[out] as [money], 'out' as [type] from [outcome]
         union all
         select [point],[date],[out] as [money], 'out' as [type] from [outcome_o]
         union all 
         select [point],[date],[inc] as [money], 'inc' as [type] from [income]
         union all 
         select [point],[date],[inc] as [money], 'inc' as [type] from [income_o]
)
select  [money] as [max_sum],[type],[date],[point]
from    union_CTE
where   [money] = (select MAX([money]) from union_CTE)

--------------------------------------- 9 -------------------------------------

select *, 
         [price] - AVG([price]) OVER ( partition by [speed]) as [dif_local_price],
		 [price] - AVG([price]) OVER () as [dif_total_price],
		 AVG([price]) OVER () as [total_price]
from [pc]
