use [labor_sql]
go 
------------------ 1  -----------------------

;WITH ship_cte     ([name],[class],[type],[country],[lanched],[displacement]) as
(
select              s.[name],s.[class],c.[type],c.[country],s.[launched],c.[displacement]
from                [classes] c inner join [ships] s on c.[class] = s.[class]
 ) 

select              s.*, o.[battle],o.[result]
from                [ship_cte] s inner join [outcomes] o on s.[name] = o.[ship]
where               [result] like 'sunk'
         
------------------ 2  -----------------------

;WITH ship_cte     ([name],[class],[type],[country],[lanched],[displacement]) as
(
select              s.[name],s.[class],c.[type],c.[country],s.[launched],c.[displacement]
from                [classes] c inner join [ships] s on c.[class] = s.[class]
 ) 
,
ship_name_cte ([name]) as
(
select              [ship] as [name]
from                [outcomes]
where               [ship] not in (select [name] from [ship_cte])  
)
select              * 
from                [ship_name_cte]

------------------ 3  -----------------------

select              rg.[id] as [region_id],lg.[id] as [place_id], lg.[name], 1 as [PlaceLevel]
from                [geography] lg inner join [geography] rg on lg.[region_id] = rg.[id]
where               rg.[id]   in (select [id] 
                                  from   [geography] 
								  where  [region_id] is null)

------------------ 4  -----------------------

;WITH geo_tree_cte ([region_id],[palce_id],[name],[place_level]) as
(
      select        [region_id],[id] as [place_id],[name], 0 as [place_level]
      from          [geography]
	  where         [region_id] = 2
	  union all
	  select        g.[region_id],g.[id] as [place_id],g.[name], t.[place_level] + 1
      from          [geography] g inner join [geo_tree_cte] t on g.[region_id] = t.[palce_id]
	  
)
select * from geo_tree_cte order by place_level

------------------ 5  -----------------------

;WITH 
ten_CTE   ([x]) as
(
select     [x] = 1
union all
select     [x] + 1 
from       ten_CTE 
where      x < 10000
)
select     [x]     
from       ten_CTE
option    (MAXRECURSION 0)

------------------ 6  -----------------------

;WITH 
ten_CTE    ([x]) as
(
select      [x] = 1
union all
select      [x] + 1 
from        ten_CTE 
where       x < 100000
)
select      [x]     from ten_CTE 
option     (MAXRECURSION 0)

------------------ 7  -----------------------

declare  @start_date DateTime, @end_date DateTime
select   @start_date  = '20180101', @end_date  = '20181231'

;WITH    [date_result]([date]) AS (
select   @start_date  
union all
select   DATEADD(day, 1, [date]) [date]
from     [date_result] 
where    [date] < @end_date
)

select    d.[day], count(*) as [amount]
from      (select [date], 'Sat' as[day] from [date_result] where DATEPART(dw,[date]) = 7
           union
           select [date], 'Sun' as[day] from [date_result] where DATEPART(dw,[date]) = 1) d
group by  d.[day] 
option   (MAXRECURSION 0);

------------------ 8  -----------------------
; WITH  
pc_product_CTE           AS(
select distinct [maker] 
from            [product]
where           [type] like 'pc'
)
,
laptop_product_CTE([maker]) AS(
select distinct [maker]
from            [product]
where           [type] like 'laptop'
)
select          [maker]
from            [pc_product_CTE]           
where           [maker] not in (select * from [laptop_product_CTE])  
        
------------------ 9  -----------------------

; WITH  
pc_product_CTE           AS(
select distinct [maker] 
from            [product]
where           [type] like 'pc'
)
,
laptop_product_CTE([maker]) AS(
select distinct [maker]
from            [product]
where           [type] like 'laptop'
)
select          [maker]
from            [pc_product_CTE]           
where           [maker] <> ALL (select * from [laptop_product_CTE])  

------------------ 10 -----------------------

; WITH  
pc_product_CTE           AS(
select distinct [maker] 
from            [product]
where           [type] like 'pc'
)
,
laptop_product_CTE([maker]) AS(
select distinct [maker]
from            [product]
where           [type] like 'laptop'
)
select          [maker]
from            [pc_product_CTE]           
where       not [maker] < ANY (select * from [laptop_product_CTE])  

------------------ 11 -----------------------

; WITH  
pc_product_CTE           AS(
select distinct [maker] 
from            [product]
where           [type] like 'pc'
)
,
laptop_product_CTE([maker]) AS(
select distinct [maker]
from            [product]
where           [type] like 'laptop'
)
select distinct [maker]
from            [product]
where           [maker] in (select p.[maker] 
                            from pc_product_CTE p inner join laptop_product_CTE l on p.maker = l.maker)

------------------ 12 -----------------------
; WITH  
pc_product_CTE           AS(
select distinct [maker] 
from            [product]
where           [type] like 'pc'
)
,
laptop_product_CTE([maker]) AS(
select distinct [maker]
from            [product]
where           [type] like 'laptop'
)
select distinct [maker]
from            [product]
where       not [maker] <> ALL (select p.[maker] 
                            from pc_product_CTE p inner join laptop_product_CTE l on p.maker = l.maker)

------------------ 13 -----------------------

; WITH  
pc_product_CTE           AS(
select distinct [maker] 
from            [product]
where           [type] like 'pc'
)
,
laptop_product_CTE([maker]) AS(
select distinct [maker]
from            [product]
where           [type] like 'laptop'
)
select distinct [maker]
from            [product]
where           [maker] = ANY (select p.[maker] 
                            from pc_product_CTE p inner join laptop_product_CTE l on p.maker = l.maker)

------------------ 14 -----------------------

; WITH  
pc_product_CTE           AS(
select          [maker],[model] 
from            [product]
where           [type] in ('pc')
)
,
pc_product_pc_CTE        AS(
select distinct pl.[maker],pl.[model] as [product_model],pr.[model] as [pc_model]
from            pc_product_CTE pl left join [pc] pr on pl.[model] = pr.[model] 
)
select distinct [maker]
from            pc_product_CTE
where           [maker] not in (select [maker]
                                from   pc_product_pc_CTE
							    where  [pc_model] is null )

------------------ 15 -----------------------

select          [country],[class] 
from            [classes] 
where           [class] = ALL (select  [class] 
                               from    [classes] 
							   where   [country] like 'Ukraine')

------------------ 16 -----------------------

select lo.[ship],lo.[battle],bl.[date] 
from   [outcomes] lo inner join [outcomes] ro on lo.[ship] = ro.[ship]
                     inner join [battles] bl  on lo.[battle] = bl.[name]
					 inner join [battles] br  on ro.[battle] = br.[name]
where  bl.[date] < br.[date] and lo.[result] like 'damaged'

------------------ 17 -----------------------

; WITH  
pc_product_CTE           AS(
select          [maker],[model] 
from            [product]
where           [type] = ('pc')
)
,
pc_product_pc_CTE        AS(
select distinct pl.[maker],pl.[model] as [product_model],pr.[model] as [pc_model]
from            pc_product_CTE pl left join [pc] pr on pl.[model] = pr.[model] 
)

select distinct [maker]
from            pc_product_pc_CTE a
where         not EXISTS (select 1
                                from   pc_product_pc_CTE b
							    where  a.[maker] = (select distinct [maker] from pc_product_pc_CTE where [pc_model] is null)  )

------------------ 18 -----------------------

select  distinct p.[maker]
from    [product] p inner join [pc] pc on p.[model] = pc.[model]
                    inner join [product] rp on p.[maker] = rp.[maker]
where   p.[type] > rp.[type] and pc.[speed] = (select MAX(pc.[speed]) from pc)

------------------ 19 -----------------------

select CASE 
           when s.[ship_class] is null then s.[class_class]
		   else s.[ship_class]                         
       END as class
from   (select  o.[ship], s.[class] as [ship_class],c.[class] as [class_class]
        from    [outcomes] o left join [ships] s   on o.[ship] = s.[name]
                             left join [classes] c on o.[ship] = c.[class]
        where   o.[result] like 'sunk') s
where   s.[ship_class] is not null or s.[class_class] is not null

------------------ 20 -----------------------

select    p.[maker],s.[price]
from     [product] p join (select [model],[price]
                   from   [printer]
                   where  [price] = (select max([price]) from [printer])) s  on p.[model] = s.[model]

------------------ 21 -----------------------

select    'laptop' as [type],[model],[speed]
from      [laptop]
where     [speed] < (select MIN([speed])
                     from       [pc])

------------------ 22 -----------------------

select     pl.[maker], pr.[price]
from       [product] pl inner join [printer] pr on pl.[model] = pr.[model]
where      pl.[model] in (select [model] 
                          from   [printer] 
						  where  [color] like 'y' 
						  and
						      pr.[price] = (select MIN([price]) 
							                from       [printer] 
											where      [color] like 'y'))

------------------ 23 -----------------------

select      [battle],[country], COUNT(*) as [amount]
from       (select o.[battle], c.[country]
            from   [outcomes] o inner join [ships]   s on o.[ship]  = s.[name]
                                inner join [classes] c on s.[class] = c.[class]) c_b
group by   [battle],[country]
having     COUNT(*) >= 2

------------------ 24 -----------------------

select     [piv].[maker],
    isNULL([piv].[pc],0)      as pc,
	isNULL([piv].[laptop],0)  as laptop,
	isNULL([piv].[printer],0) as printer
from       (select p.[maker], p.[type], case when  pc.[model] is null  and
                                                   lp.[model] is null  and
											       pr.[model] is null then 0 
                                              else  1
							             end  as [amount]
            from   [product] p left join [pc]      pc on p.[model] = pc.[model]
			                   left join [laptop]  lp on p.[model] = lp.[model]
							   left join [printer] pr on p.[model] = pr.[model]
             ) pc
PIVOT (SUM(pc.[amount])
       for pc.[type] in ([pc],[laptop],[printer])) as [piv]    

------------------ 25 -----------------------

select      [maker], CASE when SUM([pc]) > 0 then CONCAT('yes ( ',SUM([pc]),' )')
                          else 'no'
                     END  as [PC]
from (      select p.[maker], p.[type], case when  pc.[model] is null then 0
                                 else  1
    			             end  as [pc]
            from   [product] p left join [pc] pc on p.[model] = pc.[model]) mk
group by   [maker]		   
               
------------------ 26 -----------------------

select       o.[point],o.[date],isNULL(i.[inc],0) as [income],isNULL(o.[out],0) as [outcome]
from         [outcome_o] o left join [income_o] i on o.[point] = i.[point] and o.[date] = i.[date]


------------------ 27 -----------------------

select    s.[name], c.[numGuns], c.[bore], c.[displacement], c.[type], c.[country], s.[launched], s.[class]
from        [ships] s inner join [classes] c on s.[class] = c.[class]
where (select (CASE when c.[numGuns] = 8          then 1
                    else 0	   
	           END) +
	          (CASE when c.[bore] = 15            then 1 
	                else 0
		       END) + 
		      (CASE when c.[displacement] = 32000 then 1 
	                else 0
		       END) + 
		      (CASE when c.[type] = 'bb'          then 1 
	                else 0
		       END) + 
		      (CASE when c.[country] = 'USA'      then 1 
	                else 0
		       END) + 
		     (CASE when s.[launched] = 1915       then 1 
	                else 0
		       END) + 
		     (CASE when s.[class] = 'Kon'         then 1 
	                else 0
		       END)) >= 4

------------------ 28 -----------------------

select o.[point], o.[date], CASE
                               when o.[sum_out] > o_o.[out] then 'more then once a day'
							   when o.[sum_out] < o_o.[out] then 'once a day'
							   else 'both' 
							END as [result]
from (select   [point],[date],sum([out]) as [sum_out] 
      from     [outcome]
      group by [point],[date]) o join [outcome_o] o_o on o.[point] = o_o.[point] and o.[date] = o_o.[date]

------------------ 29 -----------------------

select   m.[maker],m.[model],m.[type],t.[price]
from     (select [model], [price] from [pc]
          union all
		  select [model], [price] from [printer]
		  union all
		  select [model], [price] from [laptop]
          ) t join (select * from [product] where [maker] = 'B' ) m  on t.[model] = m.[model]

------------------ 30 -----------------------

select s.[ship]
from   (select [name] as [ship] from [ships]
        union 
		select distinct [ship]  from [outcomes] ) s
		join 
	   (select distinct [class] from [ships]
		union
		select [class] from [classes] ) c on s.[ship] = c.[class]
-- OR ---
select s.[ship]
from   (select [name] as [ship] from [ships]
        union 
		select distinct [ship]  from [outcomes] ) s
		,
	   (select distinct [class] from [ships]
		union
		select [class] from [classes] ) c 
where s.[ship] = c.[class]

------------------ 31 -----------------------

select [class], count(*) as amount
from (
       select [name],[class] from [ships]
       union 
       select s.[ship] as [name], c.[class] 
       from   [outcomes] s join [classes] c on s.[ship] = c.[class]) as [ships]
group by [class]
having count(*) = 1

------------------ 32 -----------------------

select [name] from [ships] where [launched] < 1942
union 
select distinct [ship] 
from   [outcomes] o join [battles] b on o.[battle] = b.[name]
where  YEAR(b.[date]) <= 1942


