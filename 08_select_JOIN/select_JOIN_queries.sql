use [labor_sql]
go
------------------- 1  ---------------------

select          m.[maker],m.[type],p.[speed],p.[hd]
from            [pc] p inner join [product] m on p.[model] = m.[model]
where           p.[hd] < 8

------------------- 2  ---------------------

select distinct m.[maker]
from            [pc] p inner join [product] m on p.[model] = m.[model]
where           p.[speed] >= 600

------------------- 3  ---------------------

select distinct p.[maker]
from            [laptop] l inner join [product] p on l.[model] = p.[model]
where           l.[speed] <= 500  

------------------- 4  ---------------------

select          ll.[model], lr.[model], ll.[hd], ll.[ram]
from            [laptop] ll inner join [laptop] lr on ll.[hd] = lr.[hd] and ll.[ram] = lr.[ram]
where           ll.[model] > lr.[model] or ll.[speed] > lr.[speed] or ll.[price] > lr.[price]
-- �������� [model] �� ���������� ���� ���, � ���� �� ������� �� �� ���� ���:
-- where        ll.[model] > lr.[model]
  
------------------- 5  ---------------------

select          cl.[country], cl.[type],cr.[type]
from            [classes] cl inner join [classes] cr on cl.[country] = cr.[country]
where           cl.[type] < cr.[type]

------------------- 6  ---------------------

select distinct [pc].[model], p.[maker]
from            [pc] inner join [product] p on [pc].[model] = p.[model]
where           [pc].[price] < 600

------------------- 7  ---------------------

select distinct pr.[model], p.[maker]
from            [printer] pr inner join [product] p on pr.[model] = p.[model]
where           pr.[price] > 300

------------------- 8  ---------------------

select          [p].[maker],[pc].[model],[pc].[price]
from            [pc] inner join [product] p on [pc].[model] = p.[model]

------------------- 9  ---------------------

select distinct p.[maker], p.[model], [pc].[price]
from            [product] p left outer join [pc] on p.[model] = [pc].[model]
where           p.[type] = 'PC' 
 
------------------- 10 ---------------------

select distinct p.[maker], p.[type], l.[model], l.[speed]
from            [laptop] l inner join [product] p on l.[model] = p.[model]
where           l.[speed] > 600

------------------- 11 ---------------------

select         s.[name],c.[displacement]
from           [ships] s inner join [classes] c on s.[class] = c.[class]

------------------- 12 --------------------- 

select         o.[ship],o.[battle],o.[result],b.[date]
from           [outcomes] o inner join [battles] b on o.[battle] = b.[name]
where          o.[result] not like 'sunk'

------------------- 13 ---------------------

select         s.[name], s.[class],s.[launched],c.[country]
from           [ships] s inner join [classes] c on s.[class] = c.[class]

------------------- 14 ---------------------

select distinct c.[name]
from            [trip] t inner join [company] c on t.[id_comp] = c.[id_comp]
where           t.[plane] like 'Boeing'

------------------- 15 ---------------------

select          p.[name],pt.[date]
from            [passenger] p inner join [pass_in_trip] pt on p.[id_psg] = pt.[id_psg]

------------------- 16 ---------------------

select          [pc].[model],[speed],[hd]
from            [pc] inner join [product] p on [pc].[model] = p.[model]
where           p.[maker] like 'A' and ([hd] = 20 or [hd] = 10)
order by        [speed]

------------------- 17 ---------------------

select         [maker], isnull([pc],0) as pc, isnull([laptop],0) as laptop, isnull([printer],0) as printer
from            (select [maker],[type], 1 as [amount] from [product]) as [table]
pivot           (SUM([amount])
                 for [type] in ([pc],[laptop],[printer])) as [pivot] 

------------------- 18 ---------------------

select          *
from            (select [price],[screen]
                 from   [laptop]) as [table] 
pivot           (AVG([price])
                 for [screen] in ([11],[12],[14],[15])) as [pivot]
 
------------------- 19 ---------------------

select          l.*, c.[maker]
from            [laptop] l cross apply (select [maker]
                                        from   [product] p
										where  l.[model] = p.[model]) c

------------------- 20 ---------------------

select          ll.[code],ll.[model],ll.[speed],ll.[ram],ll.[hd],ll.[price],ll.[screen],c.[max_price]
from            (select l.*,p.[maker]
                 from [laptop] l inner join [product] p on l.[model] = p.[model]) ll 
                 cross apply 
				(select  t.[max_price]
                 from    (select p.[maker], MAX(l.[price]) as max_price
                          from   [laptop] l inner join [product] p on l.model = p.model
                          group by p.maker) t
				 where   ll.[maker] = t.[maker]) c

------------------- 21 ---------------------
go

 CREATE FUNCTION dbo.Get_Next_Row
(
   @rn int 
)
  RETURNS TABLE
AS
RETURN
    SELECT TOP 1 *
    FROM (select *,row_number()over (order by model,code) rn from laptop)  A
    WHERE rn > @rn
    ORDER BY rn

go
select   *
from     (select *,row_number()over (order by model,code) rn from laptop)   x 
          cross apply dbo.Get_Next_Row(x.rn) y
------------------- 22 ---------------------
 
select   *           
from     (select *,row_number()over (order by model,code) rn from laptop)   x 
         outer apply dbo.Get_Next_Row(x.rn) y
go
------------------- 23 ---------------------

select          [maker],a.[model],a.[type]
from            [product] p cross apply ( select * 
                                          from   ( select top 3 [model], [type]
										           from         [product]
												   where        [type] like 'laptop'
												   union
												   select top 3 [model], [type]
										           from         [product]
												   where        [type] like 'pc'
												   union
												   select top 3 [model], [type]
										           from         [product]
												   where        [type] like 'printer'
												   ) g
										  where p.[model] = g.[model]) a
order by [type]
------------------- 24 ---------------------
select         ll.[code], lr.[name], lr.[value] 
from           [laptop] ll  cross apply ( values (ll.[hd], 'hd'),
                                                 (ll.[ram], 'ram'),
                                                 (ll.[speed], 'speed'),
												 (ll.[screen],'screen')) lr ([value],[name])
where    lr.[value] IS NOT NULL 
order by [code],[name]

 